import React, {Fragment} from 'react'
import styles from './styles'
import env from './config'
import {
  View,
  Text,
  Button,
  TextInput,
  PermissionsAndroid,
  Clipboard,
  ToastAndroid,
} from 'react-native'
import downloadManager from "react-native-simple-download-manager"

export default class App extends React.Component {

  constructor(props){
    super(props)
    this.state = { 
      text: '',
      IsDownloading: false,
    }
  }

  linkHandle = (link) => {
    return link
    .replace('https://', '')
    .replace('http://', '')
    .replace('youtube.com/', '')
    .replace('youtube.com.br/', '')
  }

  onPasteButton = () => {
    Clipboard.getString().then((clipboardText) => {
      this.setState({ text: clipboardText})
    })
  }

  onPressButton = () => {

    this.setState({IsDownloading : true})

    try {
      const granted = PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          'title': 'Youtube Downloader',
          'message': 'We need this permission to save music in your storage'
        }
      )
    } catch (err) {
      console.warn(err)
      this.setState({IsDownloading : false})
    }

    let linkId = this.linkHandle(this.state.text)
    const url = env.serverUrl + linkId;
    const config = {
      downloadTitle: linkId,
      downloadDescription:
        "Description that should appear in Native Download manager",
      saveAsName: linkId + ".mp3",
      allowedInRoaming: true,
      allowedInMetered: true,
      showInDownloads: true,
      external: true, //when false basically means use the default Download path (version ^1.3)
      path: "Download/Music/" //if "external" is true then use this path (version ^1.3)
    }
  
    downloadManager
    .download(url, {}, config)
    .then(response => {
      ToastAndroid.show("Download success!", ToastAndroid.SHORT);
      this.setState({IsDownloading : false})
    })
    .catch(err => {
      ToastAndroid.show('Download failed!" + err', ToastAndroid.SHORT);
      this.setState({IsDownloading : false})
    })
  }

  render(){
    return (
      <Fragment>
        <View style={styles.title}>
            <Text style={styles.titleText}>Youtube Downloader</Text>
        </View>
        <View style={styles.container}>
          <View style={styles.infoContainer}>
            <Text style={styles.defaultText}>
              This is an experimental music downloader from youtube. This application uses a private server to encode youtube video to mp3 music.
              Paste the youtube link here and click in 'Download Me!' button.
              Developed by rvenson (2019)
            </Text>
          </View>
          <TextInput
            style={styles.textInput}
            editable = {true}
            autoCapitalize = 'none'
            placeholder = "Type your link here"
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
          />
          <View style={styles.buttonContainer}>
            <Button 
              title="Download Me!"
              disabled={this.state.IsDownloading}
              onPress={this.onPressButton}
              accessibilityLabel="Click to download the link"
            />
            <Button
              title="Paste from Clipboard"
              disabled={this.state.IsDownloading}
              onPress={this.onPasteButton}
              accessibilityLabel="Paste a link from clipboard"
              color='tomato'
            />
          </View>
        </View>
      </Fragment>
    )
  }

}